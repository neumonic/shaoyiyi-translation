# How to Get a Promotion
如何获得晋升
To be promoted to a role, act out that role first.
为了被提升到一个角色，你先要付诸行动。
To get promoted to a title, find out what is expected of that title and do that.
为了晋升到一个头衔，你要找出这个头衔被寄予的期望并且去实现它。
To get a pay raise, negotiate armed with information.
为了涨工资，你要用知识来充实（自己与领导的）谈判
If you feel like you are past due for a promotion, talk to your boss about it. Ask them explicitly what you need to do to get promoted, and try to do it. This sounds trite, but often times your perception of what you need to do will differ considerably from your boss's. Also this will pin your boss down in some ways.
如果你感觉自己有能力升职却惨遭淘汰，你应该跟你的老板好好谈谈。明白地问问他们你需要做什么才能升职，并且努力去做。这听起来很老套，但是大多数时候你觉得自己该做的事情，与你的老板认为你要去做的事情，存在着相当大的区别。当然这种方式在某种程度上会使你的老板受到约束。
Most programmers probably have an exaggerated sense of their relative abilities in some ways---after all, we can't all be in the top 10%! However, I have seen some people who were seriously unappreciated. One cannot expect everyone's evaluation to perfectly match reality at all times, but I think people are generally moderately fair, with one caveat: you cannot be appreciated without visibility into your work. Sometimes, due to happenstance or personal habits, someone will not be noticed much. Working from home a lot or being geographically separated from your team and boss makes this especially difficult.
大多数程序员在很多时候对自己的相对能力应该都有一种夸张的认识--毕竟，我们没办法总是成为前10%！但是，我见过很多怀才不遇的人。一个人不能总是期望他人的评估与自己的现状完全吻合，但是我认为人通常是比较公平的，除非你真的没有实力才不被欣赏。有时，由于突发情况或个人习惯，有些人往往不容易被过多地注意到。如果你总是在家工作，与你的团队分隔两地，你的老板也会让你的升职变得格外困难。
Next [Serving Your Team - How to Develop Talent](../Serving-Your-Team/01-How to Develop Talent.md)
下一篇【服务你的团队-如何培养人才】